Plagiarism Checker
==================

.. image:: https://codecov.io/gl/ayan-b/plagiarism-checker/branch/master/graph/badge.svg
  :target: https://codecov.io/gl/ayan-b/plagiarism-checker

.. image:: https://gitlab.com/ayan-b/plagiarism-checker/badges/master/build.svg
  :target: https://gitlab.com/ayan-b/plagiarism-checker/pipelines


This is a plagiarism checker tool which is used as a plug-in for Yaksh. This
package is a modified version of `pycode_similar` which uses Python AST
representation to check similarity between codes.


Credits
-------

The project uses `pycode_similar` hosted on GitHub.You can look at the source
here:

 https://github.com/fyrestone/pycode_similar

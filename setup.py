from setuptools import setup

dic = {}
exec(open('pycode_similar/pycode_similar.py').read(), dic)
VERSION = '0.0.1'


if __name__ == '__main__':
    setup(name='pycode_similar',
          version=VERSION,
          description='A plagiarism checker tool for python for Yaksh',
          long_description=open('README.rst').read(),
          original_author='fyrestone',
          original_author_email='fyrestone@outlook.com',
          url='https://gitlab.com/ayan-b/plagiarism-checker',
          license="MIT License",
          package_dir={'': 'pycode_similar'},
          py_modules=['pycode_similar'],
          keywords="python code similarity plagiarism moss generic utility\
                    yaksh",
          platforms=["All"],
          classifiers=['Development Status :: 5 - Production/Stable',
                       'Intended Audience :: Developers',
                       'License :: OSI Approved :: MIT License',
                       'Natural Language :: English',
                       'Operating System :: OS Independent',
                       'Programming Language :: Python :: 2',
                       'Programming Language :: Python :: 3',
                       'Topic :: Software Development :: Libraries',
                       'Topic :: Utilities'],
          entry_points={
              'console_scripts': [
                  'pycode_similar = pycode_similar:main',
              ],
          },
          test_suite='tests',
          zip_safe=False)

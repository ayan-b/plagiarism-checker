import os
import sys
from textwrap import dedent
import unittest

sys.path.insert(0, os.path.realpath(os.path.abspath(os.path.join(
    os.path.dirname(__file__), '..'))))

import pycode_similar


class TestCases(unittest.TestCase):

    def test_basic_detect(self):
        s1 = """\
                def foo(a):
                    if a > 1:
                        return True
                    return False
             """
        s2 = """\
                class A(object):
                    def __init__(self, a):
                        self._a = a

                    def bar(self):
                        if self._a > 2:
                            return True
                        return False
             """
        result = pycode_similar.detect([dedent(s1), dedent(s2)])
        self.assertGreater(result[0][1][0].plagiarism_percent, 0.5)

    def test_basic_detect_dict(self):
        s1 = """\
                def foo(a):
                    if a > 1:
                        return True
                return False
             """
        s2 = """\
                class A(object):
                    def __init__(self, a):
                        self._a = a

                    def bar(self):
                        if self._a > 2:
                            return True
                        return False
             """
        result = pycode_similar.detect({'a': dedent(s1), 'b': dedent(s2)})
        self.assertGreater(result[0][1][0].plagiarism_percent, 0.5)
        self.assertEqual(result[0][0], 'b')

    def test_name(self):
        s1 = """\
                def foo(a):
                    if a > 1:
                        return True
                    return False
             """
        s2 = """\
                def bar(b):
                    if b > 1:
                        return True
                    return False
             """
        result = pycode_similar.detect([dedent(s1), dedent(s2)])
        self.assertEqual(result[0][1][0].plagiarism_percent, 1)

    def test_name_dict(self):
        s1 = """\
                def foo(a):
                    if a > 1:
                        return True
                    return False
             """
        s2 = """\
                def bar(b):
                    if b > 1:
                        return True
                    return False
             """
        result = pycode_similar.detect({'a': dedent(s1), 'b': dedent(s2)})
        self.assertEqual(result[0][1][0].plagiarism_percent, 1)
        self.assertEqual(result[0][0], 'b')

    def test_equal(self):
        s1 = """\
                def foo(a):
                    if a == 1:
                        return True
                    return False
            """
        s2 = """\
                def bar(b):
                    if 1 == b:
                        return True
                    return False
            """
        result = pycode_similar.detect([dedent(s1), dedent(s2)])
        self.assertEqual(result[0][1][0].plagiarism_percent, 1)

    def test_equal_dict(self):
        s1 = """\
                def foo(a):
                    if a == 1:
                        return True
                    return False
            """
        s2 = """\
                def bar(b):
                    if 1 == b:
                        return True
                    return False
            """
        result = pycode_similar.detect({'a': dedent(s1), 'b': dedent(s2)})
        self.assertEqual(result[0][1][0].plagiarism_percent, 1)
        self.assertEqual(result[0][0], 'b')

    def test_gt_lt(self):
        s1 = """\
                def foo(a):
                    if a > 1:
                        return True
                    return False
            """
        s2 = """\
                def bar(b):
                    if 1 < b:
                        return True
                    return False
            """
        result = pycode_similar.detect([dedent(s1), dedent(s2)])
        self.assertEqual(result[0][1][0].plagiarism_percent, 1)

    def test_gt_lt_dict(self):
        s1 = """\
                def foo(a):
                    if a > 1:
                        return True
                    return False
            """
        s2 = """\
                def bar(b):
                    if 1 < b:
                        return True
                    return False
            """
        result = pycode_similar.detect({'a': dedent(s1), 'b': dedent(s2)})
        self.assertEqual(result[0][1][0].plagiarism_percent, 1)
        self.assertEqual(result[0][0], 'b')

    def test_gte_lte(self):
        s1 = """\
                def foo(a):
                    if a >= 1:
                        return True
                    return False
            """
        s2 = """\
                def bar(b):
                    if 1 <= b:
                        return True
                    return False
            """
        result = pycode_similar.detect([dedent(s1), dedent(s2)])
        self.assertEqual(result[0][1][0].plagiarism_percent, 1)

    def test_gte_lte_dict(self):
        s1 = """\
                def foo(a):
                    if a >= 1:
                        return True
                    return False
            """
        s2 = """\
                def bar(b):
                    if 1 <= b:
                        return True
                    return False
            """
        result = pycode_similar.detect({'a': dedent(s1), 'b': dedent(s2)})
        self.assertEqual(result[0][1][0].plagiarism_percent, 1)
        self.assertEqual(result[0][0], 'b')

    def test_space_and_comments(self):
        s1 = """\
                def foo(a):
                    \"""
                    foo comments.
                    \"""
                    if a >= 1:
                        return True

                    # this should return False
                    return False
            """
        s2 = """\
                def bar(b):
                # bar comments.
                    if 1 <= b:
                        \"""
                        This should
                        return True
                        \"""
                        return True
                    return False
            """
        result = pycode_similar.detect([dedent(s1), dedent(s2)])
        self.assertEqual(result[0][1][0].plagiarism_percent, 1)

    def test_space_and_comments_dict(self):
        s1 = """\
                def foo(a):
                    \"""
                    foo comments.
                    \"""
                    if a >= 1:
                        return True

                    # this should return False
                    return False
            """
        s2 = """\
                def bar(b):
                # bar comments.
                    if 1 <= b:
                        \"""
                        This should
                        return True
                        \"""
                        return True
                    return False
            """
        result = pycode_similar.detect({'a': dedent(s1), 'b': dedent(s2)})
        self.assertEqual(result[0][1][0].plagiarism_percent, 1)
        self.assertEqual(result[0][0], 'b')

    def test_expr(self):
        s1 = """\
                def foo(a):
                    yield c
             """
        s2 = """\
                def bar(b):
                    yield a
             """
        result = pycode_similar.detect([dedent(s1), dedent(s2)])
        self.assertEqual(result[0][1][0].plagiarism_percent, 1)

    def test_expr_dict(self):
        s1 = """\
                def foo(a):
                    yield c
            """
        s2 = """\
                def bar(b):
                    yield a
            """
        result = pycode_similar.detect({'a': dedent(s1), 'b': dedent(s2)})
        self.assertEqual(result[0][1][0].plagiarism_percent, 1)
        self.assertEqual(result[0][0], 'b')

    def test_no_function(self):
        s1 = """\
                def foo(a):
                    c = a
            """
        s2 = """\
                class B(object):
                    pass
             """
        try:
            result = pycode_similar.detect([dedent(s2), dedent(s1)])
        except Exception as ex:
            self.assertEqual(ex.source, 0)
        result = pycode_similar.detect([dedent(s1), dedent(s2)])
        self.assertEqual(result[0][1][0].plagiarism_percent, 0)

    def test_no_function_dict(self):
        s1 = """\
                def foo(a):
                    c = a
            """
        s2 = """\
                class B(object):
                    pass
            """
        try:
            result = pycode_similar.detect([dedent(s2), dedent(s1)])
        except Exception as ex:
            self.assertEqual(ex.source, 0)
        result = pycode_similar.detect({'a': dedent(s1), 'b': dedent(s2)})
        self.assertEqual(result[0][1][0].plagiarism_percent, 0)
        self.assertEqual(result[0][0], 'b')

    def test_strip_print(self):
        s1 = """\
                def foo(a):
                    a = b
                    print('abc', a)
            """
        s2 = """\
                def foo(a):
                    print('abc', bar())
                    a = b
            """

        result = pycode_similar.detect([dedent(s1), dedent(s2)])
        self.assertEqual(result[0][1][0].plagiarism_percent, 1)

    def test_strip_print_dict(self):
        s1 = """\
                def foo(a):
                    a = b
                    print('abc', a)
            """
        s2 = """\
                def foo(a):
                    print('abc', bar())
                    a = b
            """

        result = pycode_similar.detect({'a': dedent(s1), 'b': dedent(s2)})
        self.assertEqual(result[0][1][0].plagiarism_percent, 1)
        self.assertEqual(result[0][0], 'b')
